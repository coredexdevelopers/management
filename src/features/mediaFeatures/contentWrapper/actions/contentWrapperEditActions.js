import { EDIT_CONTENT_WRAPPER, EDIT_CONTENT_WRAPPER_FAILED } from './types';

const host = process.env.REACT_APP_MAIN_HOST;

export function editContentWrapperAsync(token, contentWrapper, newTitle, newDescription){
  const myHeaders = new Headers();
  myHeaders.append('Authorization',"Bearer " + token);

  var formData = new FormData();
  formData.append('title', newTitle);
  formData.append('description', newDescription);
  formData.append('_method', 'PUT');

  return dispatch => {
    return fetch(host+`/contentwrappers/${contentWrapper.id}`,
      {
        method: 'post',
        body: formData,
        headers: myHeaders
      }
    )
    .then(response => {
      if (response.ok) {
        response.json()
        .then(json => {dispatch(editContentWrapper(json))});
        return new Promise((resolve, reject) => { resolve() });
      } else {
        response.text()
        .then((json) => {throw JSON.parse(json).error})
        .catch((error) => dispatch(editContentWrapperFailed(error)));
        return new Promise((resolve, reject) => { reject() });
      }
    })
  }
}

export function editContentWrapper(contentWrappers){
  return { type: EDIT_CONTENT_WRAPPER, contentWrappers }
}
export function editContentWrapperFailed(error){
  return { type: EDIT_CONTENT_WRAPPER_FAILED, error }
}
