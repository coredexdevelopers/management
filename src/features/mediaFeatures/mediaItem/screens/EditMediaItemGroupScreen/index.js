import React, {Component} from "react";
import { connect } from 'react-redux';
import { push, goBack } from 'react-router-redux';
import './index.css';
import EditMediaItemGroupForm from '../../components/EditMediaItemGroupForm';
import * as mediaItemEditActions from '../../actions/mediaItemEditActions';
import * as mediaItemGetActions from '../../actions/mediaItemGetActions';
import * as mediaItemGroupActions from '../../actions/mediaItemGetActions';
import * as mediaItemActions from '../../actions/mediaItemActions';
import { Grid, Menu, Sticky, Message } from 'semantic-ui-react';

class EditMediaItemGroupScreen extends Component {
  constructor(props) {
    super(props);
    this.state = { saved: 0 };
  }

  componentDidMount(){
    const mediaItemGroupId = this.props.match.params.id;
    this.props.getMediaItemAsync({id: mediaItemGroupId});
  }

  componentDidUpdate(prevProps){
    if(prevProps.activeMediaItemGroup !== this.props.activeMediaItemGroup){
      this.setDefaultValues();
    }
  }


  setDefaultValues(){
    // set default values of active media item group
    const mediaItemGroup = this.props.activeMediaItemGroup;
    this.props.setNewTitle(mediaItemGroup.title);
    this.props.setNewRecordedAt(mediaItemGroup.recordedAt);
  }

  handleEditMediaItemGroupAsync(){

    const editMediaItemAsync = this.props.editMediaItemAsync;
    editMediaItemAsync(this.props.token, this.props.activeMediaItemGroup,
      null, null,
      null, null, this.props.newTitle,
      null, null, 1, this.props.newRecordedAt, null,
      null)
    .then(() => {
      this.setState({saved: this.state.saved+1});
    })
  }



  render() {
    return (
      <div className="app-container">
        <Sticky>
          <Menu>
            <Menu.Item icon="arrow left" onClick={() => this.props.goBack()}>
            </Menu.Item>
            <Menu.Item>
              تعديل مجموعة
            </Menu.Item>
          </Menu>
        </Sticky>
        {this.props.errors.length > 0 &&
          <Message negative>Errors: {this.props.errors.length}</Message>
        }
        {this.state.saved > 0 &&
          <Message positive>Saved: {this.state.saved}</Message>
        }
        <Grid>
          <Grid.Column computer={8}>
            <EditMediaItemGroupForm
              mediaItemGroup={this.props.activeMediaItemGroup}
              onNewTitle={(newTitle) => this.props.setNewTitle(newTitle)}
              newTitle={this.props.newTitle}
              onNewRecordedAt={(newRecordedAt) => this.props.setNewRecordedAt(newRecordedAt)}
              newRecordedAt={this.props.newRecordedAt}
              onSave={() => this.handleEditMediaItemGroupAsync()}
              onCancel={() => this.props.goBack()}
              />
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      activeMediaItemGroup: state.mediaItemGroup.activeMediaItemGroup,
      errors: state.mediaItem.errors,
      newTitle: state.mediaItem.newTitle,
      newRecordedAt: state.mediaItem.newRecordedAt
    };
};
const mapDispatchToProps = {
  push,
  goBack,
  ...mediaItemEditActions,
  ...mediaItemGroupActions,
  ...mediaItemGetActions,
  ...mediaItemActions

}

export default connect(mapStateToProps, mapDispatchToProps)(EditMediaItemGroupScreen);
