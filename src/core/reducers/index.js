import { combineReducers } from "redux";
import contentWrapperReducer from '../../features/mediaFeatures/contentWrapper/reducers/contentWrapperReducer';
import mediaItemReducer from '../../features/mediaFeatures/mediaItem/reducers/mediaItemReducer';
import mediaItemGroupReducer from '../../features/mediaFeatures/mediaItem/reducers/mediaItemGroupReducer';
import fileBrowserReducer from '../../features/fileBrowser/reducers/fileBrowserReducer';
import routerPreviousLocationReducer from './routerPreviousLocationReducer';

//import myReducer from "./myReducerPath";
export default combineReducers({
  contentWrapper: contentWrapperReducer,
  mediaItem: mediaItemReducer,
  mediaItemGroup: mediaItemGroupReducer,
  fileBrowser: fileBrowserReducer,
  routerPreviousLocation: routerPreviousLocationReducer
});
