import React, {Component} from "react";
import { connect } from 'react-redux';
import { push, goBack } from 'react-router-redux';
import './index.css';
import EditMediaItemForm from '../../components/EditMediaItemForm';
import * as mediaItemActions from '../../actions/mediaItemActions';
import * as mediaItemEditActions from '../../actions/mediaItemEditActions';
import * as mediaItemGetActions from '../../actions/mediaItemGetActions';
import { Message, Header, Container, Sticky, Menu, Grid, Button } from 'semantic-ui-react';
import { FilePond } from 'react-filepond';
const filepondUrl = process.env.REACT_APP_FILEPOND_HOST;
const webFtpClientUrl = process.env.REACT_APP_WEB_FTP_CLIENT_HOST;

class EditMediaItemScreen extends Component {
  constructor(props) {
    super(props);
    this.state = { saved: 0 };
  }

  componentWillMount(){
    const mediaItemId = this.props.match.params.id;
    this.props.getMediaItemAsync({id: mediaItemId});
  }

  componentDidUpdate(prevProps){
    if(prevProps.activeMediaItem !== this.props.activeMediaItem
    && this.props.activeMediaItem !== null && this.props.previousLocation !== '/fileBrowser'){
      this.setDefaultValues();
    }
  }

  setDefaultValues(){
    // set default values of active media item
    const mediaItem = this.props.activeMediaItem;
    this.props.setNewLowQualityAudio(mediaItem.lowQualityAudio);
    this.props.setNewHighQualityAudio(mediaItem.highQualityAudio);
    this.props.setNewVideo(mediaItem.video);
    this.props.setNewPdf(mediaItem.pdf);
    this.props.setNewTitle(mediaItem.title);
    this.props.setNewDescription(mediaItem.description);
    this.props.setNewRecordedAt(mediaItem.recordedAt);
    this.props.setNewThumbnail(mediaItem.thumbnail);

  }

  handleEditMediaItemAsync(){
    const editMediaItemAsync = this.props.editMediaItemAsync;
    editMediaItemAsync(this.props.token, this.props.activeMediaItem,
      this.props.newLowQualityAudio, this.props.newHighQualityAudio,
      this.props.newVideo, this.props.newPdf, this.props.newTitle,
      this.props.newDescription, this.props.newThumbnail, 0, this.props.newRecordedAt, this.props.activeMediaItemGroup?null:this.props.activeContentWrapper,
      this.props.activeMediaItemGroup)
    .then(() => {
      this.setState({saved: this.state.saved+1});
    })
  }

  getYoutubeVideoId(url) {
      var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
      var match = url.match(regExp);

      if (match && match[2].length == 11) {
          return match[2];
      } else {
          return 'error';
      }
  }


  handleSetNewVideo(newVideo){
    if(newVideo.includes('youtube') || newVideo.includes('youtu.be')){
      newVideo = "https://youtube.com/embed/"+this.getYoutubeVideoId(newVideo);
    }
    this.props.setNewVideo(newVideo);
  }


  render() {
    return (
      <div className="app-container">
        <Sticky>
          <Menu>
            <Menu.Item icon="arrow left" onClick={() => this.props.goBack()}>
            </Menu.Item>
            <Menu.Item>
              تعديل محتوى
            </Menu.Item>
            <Menu.Item>
              <Button onClick={() => this.props.push('/fileBrowser')}>الملفات - بسيط</Button>
            </Menu.Item>
            <Menu.Item>
              <Button onClick={() => window.open(webFtpClientUrl, "_blank")}>الملفات - متطور</Button>
            </Menu.Item>
          </Menu>
        </Sticky>
        {this.props.errors.length > 0 &&
          <Message negative>{this.props.errors.length + ' errors occured.'}</Message>
        }
        {this.state.saved > 0 &&
          <Message positive>{this.state.saved + ' saved successfully.'}</Message>
        }

        <Grid>
          <Grid.Column>
            <EditMediaItemForm
              mediaItem={this.props.activeMediaItem}
              onNewLowQualityAudio={(newLowQualityAudio) => this.props.setNewLowQualityAudio(newLowQualityAudio)}
              onNewHighQualityAudio={(newHighQualityAudio) => this.props.setNewHighQualityAudio(newHighQualityAudio)}
              newHighQualityAudio={this.props.newHighQualityAudio}
              onNewPdf={(newPdf) => this.props.setNewPdf(newPdf)}
              onNewVideo={(newVideo) => this.handleSetNewVideo(newVideo)}
              onNewTitle={(newTitle) => this.props.setNewTitle(newTitle)}
              onNewDescription={(newDescription) => this.props.setNewDescription(newDescription)}
              onNewThumbnail={(newThumbnail) => this.props.setNewThumbnail(newThumbnail)}
              onNewRecordedAt={(newRecordedAt) => this.props.setNewRecordedAt(newRecordedAt)}
              onSave={() => this.handleEditMediaItemAsync()}
              onCancel={() => this.props.goBack()}

              newLowQualityAudio={this.props.newLowQualityAudio}
              newHighQualityAudio={this.props.newHighQualityAudio}
              newPdf={this.props.newPdf}
              newVideo={this.props.newVideo}
              newTitle={this.props.newTitle}
              newDescription={this.props.newDescription}
              newThumbnail={this.props.newThumbnail}
              newRecordedAt={this.props.newRecordedAt}
              />
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      token: null,
      errors: state.mediaItem.errors,
      activeMediaItem: state.mediaItem.activeMediaItem,
      newTitle: state.mediaItem.newTitle,
      newDescription: state.mediaItem.newDescription,
      newThumbnail: state.mediaItem.newThumbnail,
      newLowQualityAudio: state.mediaItem.newLowQualityAudio,
      newHighQualityAudio: state.mediaItem.newHighQualityAudio,
      newVideo: state.mediaItem.newVideo,
      newPdf: state.mediaItem.newPdf,
      newRecordedAt: state.mediaItem.newRecordedAt,
      previousLocation: state.routerPreviousLocation.previousLocation
    };
};
const mapDispatchToProps = {
  ...mediaItemEditActions,
  ...mediaItemActions,
  ...mediaItemGetActions,
  push,
  goBack
}

export default connect(mapStateToProps, mapDispatchToProps)(EditMediaItemScreen);
