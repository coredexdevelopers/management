import { SET_ACTIVE_CONTENT_WRAPPER, SET_NEW_TITLE } from './types';

const host = process.env.REACT_APP_MAIN_HOST;

export function setActiveContentWrapper(activeContentWrapper){
  return { type: SET_ACTIVE_CONTENT_WRAPPER, activeContentWrapper }
}
export function setNewTitle(newTitle){
  return { type: SET_NEW_TITLE, newTitle }
}
