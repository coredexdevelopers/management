import { DELETE_CONTENT_WRAPPER, DELETE_CONTENT_WRAPPER_FAILED } from './types';

const host = process.env.REACT_APP_MAIN_HOST;

export function deleteContentWrapperAsync(token, contentWrapper){
  const myHeaders = new Headers();

  myHeaders.append('Authorization',"Bearer " + token);
  return dispatch => {
    return fetch(host + '/contentwrappers/'+contentWrapper.id, {
        method: 'delete',
        headers: myHeaders,
    })
      .then(response => {
        if (response.ok) {
          response.json()
          .then(json => {dispatch(deleteContentWrapper(json))});
          return new Promise((resolve, reject) => { resolve() });
        } else {
          response.text()
          .then((json) => {throw JSON.parse(json).error})
          .catch((error) => dispatch(deleteContentWrapperFailed(error)));
          return new Promise((resolve, reject) => { reject() });
        }
      })
  };
}
export function deleteContentWrapper(contentWrapper){
  return { type: DELETE_CONTENT_WRAPPER, contentWrapper }
}

export function deleteContentWrapperFailed(error){
  return { type: DELETE_CONTENT_WRAPPER_FAILED, error }
}
