import React, {Component} from "react";
import { connect } from 'react-redux';
import { push, goBack } from 'react-router-redux';
import './index.css';
import * as mediaItemGetActions from '../../actions/mediaItemGetActions';
import * as mediaItemActions from '../../actions/mediaItemActions';
import MediaItemBox from '../../components/MediaItemBox';
import { Sticky, Menu, Grid, Message } from 'semantic-ui-react';

class MediaItemScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount(){
    const mediaItemId = this.props.match.params.id;
    this.props.getMediaItemAsync({id: mediaItemId});
  }

  render() {
    const mediaItem = this.props.activeMediaItem;
    return (
      <div className="app-container">
        <Sticky>
          <Menu>
            <Menu.Item icon="arrow left" onClick={() => this.props.goBack()}>
            </Menu.Item>
            <Menu.Item>
              {this.props.activeMediaItem && this.props.activeMediaItem.title}
            </Menu.Item>
          </Menu>
        </Sticky>
        {this.props.errors.length > 0 &&
          <Message negative>{this.props.errors.length} errors occurred.</Message>
        }
        <Grid>
          <Grid.Column computer={16} mobile={16} tablet={16}>
            <MediaItemBox mediaItem={mediaItem} details />
          </Grid.Column>
        </Grid>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
    return {
      errors: state.mediaItem.errors,
      activeMediaItem: state.mediaItem.activeMediaItem,
    };
};
const mapDispatchToProps = {
  ...mediaItemGetActions,
  ...mediaItemActions,
  push,
  goBack
}

export default connect(mapStateToProps, mapDispatchToProps)(MediaItemScreen);
