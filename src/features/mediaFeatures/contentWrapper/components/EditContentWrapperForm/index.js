import React from 'react';
import './index.css';
import { Button, Card, Input } from 'semantic-ui-react';
const EditContentWrapperForm = ({contentWrapper, ...props}) => {
  if(!contentWrapper){
    return(
      <div>
        No content wrapper to edit.
      </div>
    )
  }
  return (
      <Card {...props}>
        <Card.Content>
          <Card.Header>
            <Input type="text" onInput={(e) => props.onNewTitle(e.target.value)}
              placeholder="العنوان"
              defaultValue={contentWrapper.title}
              />
          </Card.Header>
        </Card.Content>
        <Card.Content extra>
          <Button className="margin-bottom-gap" fluid primary onClick={() => props.onSave()}>Save</Button>
          <Button className="margin-bottom-gap" fluid secondary  onClick={() => props.onCancel()}>Cancel</Button>
        </Card.Content>
      </Card>
  );
};

export default EditContentWrapperForm;
