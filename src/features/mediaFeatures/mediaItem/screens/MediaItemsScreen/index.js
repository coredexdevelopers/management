import React, {Component} from "react";
import { connect } from 'react-redux';
import { push, goBack } from 'react-router-redux';
import './index.css';
import * as mediaItemGetActions from '../../actions/mediaItemGetActions';
import * as mediaItemActions from '../../actions/mediaItemActions';
import * as mediaItemDeleteActions from '../../actions/mediaItemDeleteActions';
import * as mediaItemEditActions from '../../actions/mediaItemEditActions';
import * as mediaItemGroupActions from '../../actions/mediaItemGroupActions';
import * as mediaItemPushNotificationActions from '../../actions/mediaItemPushNotificationActions';
import * as contentWrapperActions from '../../../contentWrapper/actions/contentWrapperActions';
import * as contentWrapperGetActions from '../../../contentWrapper/actions/contentWrapperGetActions';
import MediaItemFilter from '../../reducers/standardizers/MediaItemFilter';
import MediaItemBox from '../../components/MediaItemBox';
import AddMediaItemBox from '../../components/AddMediaItemBox';
import AddMediaItemGroupBox from '../../components/AddMediaItemGroupBox';
import { Sticky, Menu, Grid, Message } from 'semantic-ui-react';


class MediaItemsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount(){
    this.getMediaItemsBasedOnParentEntity();
    this.getParentEntity(); // fetch additional parent entity data for display purposes (we only have the ID)
  }

  // the parent entity is either contentWrapper or mediaItemGroup, as read from the URL
  getMediaItemsBasedOnParentEntity(){
    if(this.props.match.params.parentEntity === 'contentWrapper'){
      this.getMediaItemsOfContentWrapper({id: this.props.match.params.parentId});
    } else if(this.props.match.params.parentEntity === 'mediaItemGroup') {
      this.getMediaItemsOfMediaItemGroup({id: this.props.match.params.parentId});
    }
  }

  componentDidUpdate(prevProps){
    if(prevProps.match.params.parentEntity === this.props.match.params.parentEntity
    && prevProps.match.params.parentId === this.props.match.params.parentId){
      return;
    }
    // any changes to the parent entity or to the parent entity ID should trigger a refetch.
    this.getMediaItemsBasedOnParentEntity();
  }

  getMediaItemsOfContentWrapper(contentWrapper){
    const myMediaItemFilter = new MediaItemFilter();
    myMediaItemFilter.contentWrapperId = contentWrapper.id;
    this.props.getMediaItemsAsync(myMediaItemFilter);
  }

  getMediaItemsOfMediaItemGroup(mediaItemGroup){
    const myMediaItemFilter = new MediaItemFilter();
    myMediaItemFilter.groupMediaItemId = mediaItemGroup.id;
    this.props.getMediaItemsAsync(myMediaItemFilter);
  }

  getParentEntity(){
    if(this.props.match.params.parentEntity === 'contentWrapper'){
      this.props.getContentWrapperAsync({id: this.props.match.params.parentId});
    } else if(this.props.match.params.parentEntity === 'mediaItemGroup') {
      this.props.getMediaItemAsync({id: this.props.match.params.parentId});
    }
  }

  handleMediaItemBoxClick(mediaItem){
    this.props.setActiveMediaItem(mediaItem);
  }

  handleMediaItemOpen(mediaItem){
    if(mediaItem.isGroup){
      this.props.push(`/mediaItems/mediaItemGroup/${mediaItem.id}`);
    } else {
      this.props.push(`/mediaItem/${mediaItem.id}`);
    }
  }

  handleMediaItemEdit(mediaItem){
    if(mediaItem.isGroup){
      this.props.push(`/editMediaItemGroup/${mediaItem.id}`);
    } else {
      this.props.push(`/editMediaItem/${mediaItem.id}`);
    }

  }
  handleMediaItemDelete(mediaItem){
    const errors = this.props.errors;
    this.props.deleteMediaItemAsync(null, mediaItem)
    .then(() => {
      this.props.getMediaItemsAsync(this.props.mediaItemFilter);
    });
  }

  handleMediaItemPushNotify(mediaItem){
    this.props.sendPushNotificationsAsync(null, mediaItem);
  }


  render() {
    return (
      <div className="app-container">
        <Sticky>
          <Menu>
            <Menu.Item icon="arrow left" onClick={() => this.props.goBack()}>
            </Menu.Item>
            <Menu.Item>
              {this.props.activeContentWrapper && this.props.activeContentWrapper.title}
            </Menu.Item>
          </Menu>
        </Sticky>
        {
          this.props.errors.length > 0 &&
          <Message>{this.props.errors.length} errors occurred.</Message>
        }
        <Grid>
          <Grid.Column mobile={16} tablet={6} computer={4}>
            <AddMediaItemBox onClick={() => this.props.push(`/addMediaItem/${this.props.match.params.parentEntity}/${this.props.match.params.parentId}`)} />
            <AddMediaItemGroupBox onClick={() => this.props.push(`/addMediaItemGroup/${this.props.match.params.parentEntity}/${this.props.match.params.parentId}`)} />
          </Grid.Column>
          {this.props.mediaItems.map((mediaItem) =>
            <Grid.Column mobile={16} tablet={6} computer={4}>
              <MediaItemBox mediaItem={mediaItem}
                className={this.props.activeMediaItem && this.props.activeMediaItem.id == mediaItem.id ? 'mediaItemBox_active' : ''}
                onClick={() => this.handleMediaItemBoxClick(mediaItem)}
                showControls={true}
                onOpen={() => this.handleMediaItemOpen(mediaItem)}
                onEdit={() => this.handleMediaItemEdit(mediaItem)}
                onDelete={() => this.handleMediaItemDelete(mediaItem)}
                onPushNotify={() => this.handleMediaItemPushNotify(mediaItem)}
              />
            </Grid.Column>
            )
          }
          <Grid.Column mobile={16} tablet={6} computer={4}>
            <AddMediaItemBox onClick={() => this.props.push(`/addMediaItem/${this.props.match.params.parentEntity}/${this.props.match.params.parentId}`)} />
            <AddMediaItemGroupBox onClick={() => this.props.push(`/addMediaItemGroup/${this.props.match.params.parentEntity}/${this.props.match.params.parentId}`)} />
          </Grid.Column>
        </Grid>

    </div>
  )
  }
}

const mapStateToProps = (state) => {
    return {
      mediaItemFilter: state.mediaItem.mediaItemFilter,
      activeContentWrapper: state.contentWrapper.activeContentWrapper,
      mediaItems: state.mediaItem.mediaItems,
      errors: state.mediaItem.errors,
      activeMediaItem: state.mediaItem.activeMediaItem,
      activeMediaItemGroup: state.mediaItemGroup.activeMediaItemGroup
    };
};
const mapDispatchToProps = {
  ...mediaItemGetActions,
  ...mediaItemActions,
  ...mediaItemDeleteActions,
  ...mediaItemEditActions,
  ...mediaItemGroupActions,
  ...contentWrapperActions,
  ...contentWrapperGetActions,
  ...mediaItemPushNotificationActions,
  push,
  goBack
}

export default connect(mapStateToProps, mapDispatchToProps)(MediaItemsScreen);
