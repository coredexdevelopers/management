import { ADD_MEDIA_ITEM, ADD_MEDIA_ITEM_FAILED } from './types';
import { setActiveMediaItemGroup } from './mediaItemGroupActions';
import { setActiveContentWrapper } from '../../contentWrapper/actions/contentWrapperActions';

const host = process.env.REACT_APP_MAIN_HOST;



export function addMediaItemAsync(token, newLowQualityAudio, newHighQualityAudio,
newVideo, newPdf, newTitle, newDescription, newThumbnail, newIsGroup, newRecordedAt, parentContentWrapper, parentMediaItemGroup){
  const myHeaders = new Headers();
  myHeaders.append('Authorization',"Bearer " + token);

  var formData = new FormData();
  formData.append('lowQualityAudio', newLowQualityAudio);
  formData.append('highQualityAudio', newHighQualityAudio)
  formData.append('video', newVideo);
  formData.append('pdf', newPdf);
  formData.append('title', newTitle);
  formData.append('description', newDescription);
  formData.append('thumbnail', newThumbnail);
  formData.append('isGroup', newIsGroup);
  if(newRecordedAt){
    formData.append('recordedAt', newRecordedAt); // SQL error occurs if we send a null date.
  }
  if(parentContentWrapper){
    formData.append('contentWrapperId', parentContentWrapper.id);
  }
  if(parentMediaItemGroup){
    formData.append('groupMediaItemId', parentMediaItemGroup.id);
  }


  return dispatch => {
    dispatch(setActiveContentWrapper(parentContentWrapper));
    dispatch(setActiveMediaItemGroup(parentMediaItemGroup));
    return fetch(host+'/mediaitems',
      {
        method: 'post',
        body: formData,
        headers: myHeaders
      }
    )
    .then(response => {
      if (response.ok) {
        response.json()
        .then(json => {dispatch(addMediaItem(json))});
        return new Promise((resolve, reject) => { resolve() });
      } else {
        response.text()
        .then((json) => {throw JSON.parse(json).error})
        .catch((error) => dispatch(addMediaItemFailed(error)));
        return new Promise((resolve, reject) => { reject() });

      }
    })
  }
}

export function addMediaItem(mediaItem){
  return { type: ADD_MEDIA_ITEM, mediaItem }
}
export function addMediaItemFailed(error){
  return { type: ADD_MEDIA_ITEM_FAILED, error }
}
