import {
  GET_CONTENT_WRAPPERS,
  GET_CONTENT_WRAPPERS_FAILED,
  GET_CONTENT_WRAPPER,
  GET_CONTENT_WRAPPER_FAILED, // not supported
  EDIT_CONTENT_WRAPPER, // not supported
  EDIT_CONTENT_WRAPPER_FAILED,
  ADD_CONTENT_WRAPPER,
  ADD_CONTENT_WRAPPER_FAILED,
  DELETE_CONTENT_WRAPPER,
  DELETE_CONTENT_WRAPPER_FAILED,
  SET_ACTIVE_CONTENT_WRAPPER,
  SET_NEW_TITLE,
  SET_NEW_DESCRIPTION
}
 from '../actions/types';

const initialState = {
  contentWrappers: [],
  activeContentWrapper: null,
  newTitle: '',
  newDescription: '',
  errors: [],

}
const contentWrapperReducer = function(state = initialState, action) {
  switch (action.type) {
    case GET_CONTENT_WRAPPERS:
      return {
        ...state,
        contentWrappers: action.contentWrappers,
        error: []
      }
    case GET_CONTENT_WRAPPERS_FAILED:
      return {
        ...state,
        errors: [...state.errors, action.error]
      }
    case EDIT_CONTENT_WRAPPER_FAILED:
      return {
        ...state,
        errors: [...state.errors, action.error]
      }
    case ADD_CONTENT_WRAPPER:
      return {
        ...state,
        errors: []
      }
    case ADD_CONTENT_WRAPPER_FAILED:
      return {
        ...state,
        errors: [...state.errors, action.error]
      }
    case DELETE_CONTENT_WRAPPER:
      return {
        ...state,
        errors: []
      }
    case DELETE_CONTENT_WRAPPER_FAILED:
      return {
        ...state,
        errors: [...state.errors, action.error]
      }
    case SET_ACTIVE_CONTENT_WRAPPER:
      return {
        ...state,
        activeContentWrapper: action.activeContentWrapper
      }
    case SET_NEW_TITLE:
      return {
        ...state,
        newTitle: action.newTitle
      }
    default:
      return state;
  }
};

export default contentWrapperReducer;
