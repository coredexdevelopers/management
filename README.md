# Sheikh App Management App

## Setup
First of all, you need to set up your env files.
Included are two env files: `.env.development.example` and `.env.production.example`
1. Make a copy of each of them to `.env.development` and `.env.production` respectively. *DO NOT DELETE THE EXAMPLE FILES!*
2. Set the `REACT_APP_MAIN_HOST` and `REACT_APP_FILEPOND_HOST` values. Todo: explain this.
3. `npm install`
4. `npm start` to start the project in development mode.

## Coding Style and Rules

### General
- In any occurrence where CRUD actions are to be implemented, always follow this order:  
1. GET
2. PUT (edit)
3. POST (add)
4. DELETE

- In actions and reducers, start with asynchronous actions first in the order aforementioned,
followed by synchronous actions. Synchronous actions related to asynchronous actions should follow them directly.  
Example:  
```
export function getAsync(){ // start with async

}
export function getSuccess(){ // associated with getAsync, so precedes editAsync

}
export function editdAsync(){

}
export function mark(){ // not associated with an async call, and is synchronous, so is below all.

}
```
- CSS classes shall never conflict across different files. Class names should be the component name,
in camelCase, with the first letter lower-case then followed by an underscore with the intended class.  
Example:   
```
.contentWrapperBox_active {

}
```
- In reducers, `initialState` properties should be null if the property is an object,
and an empty string if the property is a string.
- In reducers, properties related to forms should be prefixed with the word `new`.
Example: `newTitle` would be a title property for the `contentWrapperAddReducer`'s state.


### Routing

- Route names are related to screens. Each route should be named the same as its screen name,
without the suffixing 'screen' and start with a lower-case letter.  
For example, for the screen `ContentWrappersScreen`, the route is `/contentWrappers`.
- Content should be guaranteed to be fetched regardless of the redux store state (and without persisting to local storage).
Hence, the URL should represent at least the items to be fetched and the foreign key filters.
The main example so far is that of media items. Media items either belong to a `contentwrapper`
or to a `mediaitemgroup`, so to make sure I can fetch the `mediaitems` of a `contentwrapper` through
the URL, it should look like this:
`mediaItems/contentWrapper/<contentWrapperId>`
The same goes for a parent `mediaitemgroup`:
`mediaItems/mediaItemGroup/<mediaItemGroupId>`
Hence, the final form of the URL would be:
`mediaItems/<parentEntity>/<parentId>`
- In light of the rule above, content MUST be fetched through the store first and the URL fetching should only be
used as a fallback in case the store fails to provide the needed data. The store must then be updated to match the
URL data.
