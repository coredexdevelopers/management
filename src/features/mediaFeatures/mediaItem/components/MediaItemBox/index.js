import React from 'react';
import './index.css';
import MediaItemDetailsList from './MediaItemDetailsList';
import { Card, Button, List } from 'semantic-ui-react';

const MediaItemBox = ({mediaItem, ...props}) => {
  if(!mediaItem){
    return (<div>No media item to display.</div>)
  }
  return (
      <Card {...props} fluid>
        <Card.Content>
          <Card.Header>
            {mediaItem.title} {mediaItem.isGroup?'(مجموعة)':''}
          </Card.Header>
        </Card.Content>
        <Card.Content extra>
          {
            props.showControls &&
            <React.Fragment>
              <Button className='content-wrapper-box-button' fluid primary onClick={() => props.onOpen()}>فتح</Button>
              <Button className='content-wrapper-box-button' fluid color="green" onClick={() => props.onPushNotify()}>إرسال إشعار للهواتف</Button>
              <Button className='content-wrapper-box-button' fluid color="violet" onClick={() => props.onEdit()}>تعديل</Button>
              <Button className='content-wrapper-box-button' fluid color="black" onClick={() => props.onDelete()}>حذف</Button>
            </React.Fragment>
          }
        </Card.Content>
        {props.details &&
          <Card.Content>
            <MediaItemDetailsList mediaItem={mediaItem} />
          </Card.Content>
        }

      </Card>
  );
};


export default MediaItemBox;
