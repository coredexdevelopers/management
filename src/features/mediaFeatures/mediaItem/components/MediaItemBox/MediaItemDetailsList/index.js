import React from 'react';
import './index.css';
import { List } from 'semantic-ui-react';
import { format } from 'date-fns';
const arLocale = require('date-fns/locale/ar');

const MediaItemDetailsList = ({mediaItem, ...props}) => {
  if(!mediaItem){
    return (<div>No media item to show details of.</div>)
  }
  return (
    <List>
      <List.Item>
      <b>وصف</b> {mediaItem.description}
      </List.Item>
      <List.Item>
      <b>صورة</b> <a href={mediaItem.thumbnail}>{mediaItem.thumbnail}</a>
      </List.Item>
      <List.Item>
      <b>مقطع فيديو</b> <a href={mediaItem.video}>{mediaItem.video}</a>
      </List.Item>
      <List.Item>
      <b>مقطع صوتي عادي الجودة</b> <a href={mediaItem.lowQualityAudio}>{mediaItem.lowQualityAudio}</a>
      </List.Item>
      <List.Item>
      <b>مقطع صوتي عالي الجودة</b> <a href={mediaItem.highQualityAudio}>{mediaItem.highQualityAudio}</a>
      </List.Item>
      <List.Item>
      <b>PDF</b> <a href={mediaItem.pdf}>{mediaItem.pdf}</a>
      </List.Item>
      <List.Item>
      <b>تاريخ التسجيل المحدد</b> {format(mediaItem.recordedAt, 'dddd D MMMM YYYY', {locale: arLocale})}
      </List.Item>
      <List.Item>
      <b>حُمِّل المقطع بتاريخ</b> {format(mediaItem.created_at, 'dddd D MMMM YYYY hh:mm a', {locale: arLocale})}
      </List.Item>
    </List>
  );
};


export default MediaItemDetailsList;
