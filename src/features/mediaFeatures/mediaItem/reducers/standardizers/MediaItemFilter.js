export default class MediaItemFilter {
  constructor(){
    this.includeVideo = true;
    this.includeAudio = true;
    this.includePdf = true;
    this.groupMediaItemId = null;
    this.contentWrapperId = null;
    this.searchQuery = null;
    this.orderBy = 'id';
  }
}
