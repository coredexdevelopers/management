import { DELETE_MEDIA_ITEM, DELETE_MEDIA_ITEM_FAILED } from './types';

const host = process.env.REACT_APP_MAIN_HOST;


export function deleteMediaItemAsync(token, mediaItem){
  const myHeaders = new Headers();
  myHeaders.append('Authorization',"Bearer " + token);
  return dispatch => {
    return fetch(host + '/mediaitems/'+mediaItem.id, {
        method: 'delete',
        headers: myHeaders,
    })
    .then(response => {
      if (response.ok) {
        response.json()
        .then(json => {dispatch(deleteMediaItem(json))});
        return new Promise((resolve, reject) => { resolve() });
      } else {
        response.text()
        .then((json) => {throw JSON.parse(json).error})
        .catch((error) => dispatch(deleteMediaItemFailed(error)));
        return new Promise((resolve, reject) => { reject() });

      }
    })
  };
}
export function deleteMediaItem(mediaItem){
  return { type: DELETE_MEDIA_ITEM, mediaItem }
}

export function deleteMediaItemFailed(error){
  return { type: DELETE_MEDIA_ITEM_FAILED, error }
}
