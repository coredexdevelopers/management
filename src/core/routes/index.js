import React from 'react';
import { Route, Link, withRouter, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import ContentWrappersScreen from '../../features/mediaFeatures/contentWrapper/screens/ContentWrappersScreen';
import AddContentWrapperScreen from '../../features/mediaFeatures/contentWrapper/screens/AddContentWrapperScreen';
import EditContentWrapperScreen from '../../features/mediaFeatures/contentWrapper/screens/EditContentWrapperScreen';

import MediaItemScreen from '../../features/mediaFeatures/mediaItem/screens/MediaItemScreen';
import MediaItemsScreen from '../../features/mediaFeatures/mediaItem/screens/MediaItemsScreen';
import AddMediaItemScreen from '../../features/mediaFeatures/mediaItem/screens/AddMediaItemScreen';
import EditMediaItemScreen from '../../features/mediaFeatures/mediaItem/screens/EditMediaItemScreen';
import AddMediaItemGroupScreen from '../../features/mediaFeatures/mediaItem/screens/AddMediaItemGroupScreen';
import EditMediaItemGroupScreen from '../../features/mediaFeatures/mediaItem/screens/EditMediaItemGroupScreen';

import FilesTableScreen from '../../features/fileBrowser/screens/FilesTableScreen';

function isAuthed(token){
  if(!token || token.length == 0){
    return false;
  }
}
function PrivateRoute ({component: Component, authed, ...rest}) {
  return (
    <Route
      {...rest}
      render={(props) => authed === true
        ? <Component {...props} />
      : <Redirect to={{pathname: process.env.REACT_APP_ROUTE_ROOT + "/contentWrappers"}} component={ContentWrappersScreen} />}
    />
  )
}

const Routes = (props) => (
    <main>
      <Route exact path={process.env.REACT_APP_ROUTE_ROOT + "/contentWrappers"} component={ContentWrappersScreen} />
      <Route exact path={process.env.REACT_APP_ROUTE_ROOT + "/addContentWrapper"} component={AddContentWrapperScreen} />
      <Route exact path={process.env.REACT_APP_ROUTE_ROOT + "/editContentWrapper/:id"} component={EditContentWrapperScreen} />

      <Route exact path={process.env.REACT_APP_ROUTE_ROOT + "/mediaItem/:id"} component={MediaItemScreen} />
      <Route exact path={process.env.REACT_APP_ROUTE_ROOT + "/mediaItems"} component={MediaItemsScreen} />
      <Route exact path={process.env.REACT_APP_ROUTE_ROOT + "/mediaItems/:parentEntity/:parentId"} component={MediaItemsScreen} />
      <Route exact path={process.env.REACT_APP_ROUTE_ROOT + "/addMediaItem/:parentEntity/:parentId"} component={AddMediaItemScreen} />
      <Route exact path={process.env.REACT_APP_ROUTE_ROOT + "/editMediaItem/:id"} component={EditMediaItemScreen} />
      <Route exact path={process.env.REACT_APP_ROUTE_ROOT + "/addMediaItemGroup/:parentEntity/:parentId"} component={AddMediaItemGroupScreen} />
      <Route exact path={process.env.REACT_APP_ROUTE_ROOT + "/editMediaItemGroup/:id"} component={EditMediaItemGroupScreen} />

      <Route exact path={process.env.REACT_APP_ROUTE_ROOT + "/fileBrowser"} component={FilesTableScreen} />
    </main>
);

const mapStateToProps = state => ({
});


export default withRouter(connect(mapStateToProps, null)(Routes));
