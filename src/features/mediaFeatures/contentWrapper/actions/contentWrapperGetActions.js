import { GET_CONTENT_WRAPPERS, GET_CONTENT_WRAPPERS_FAILED, GET_CONTENT_WRAPPER,
GET_CONTENT_WRAPPER_FAILED } from './types';
import { setActiveContentWrapper } from './contentWrapperActions';


const host = process.env.REACT_APP_MAIN_HOST;

export function getContentWrappersAsync(){
  return dispatch => {
    return fetch(host + '/contentwrappers')
    .then(response => {
      if (response.ok) {
        response.json()
        .then(json => {dispatch(getContentWrappers(json))});
        return new Promise((resolve, reject) => { resolve() });
      } else {
        response.text()
        .then((json) => {throw JSON.parse(json).error})
        .catch((error) => dispatch(getContentWrappersFailed(error)));
        return new Promise((resolve, reject) => { reject() });
      }
    })
  }
}
export function getContentWrappers(contentWrappers){
  return { type: GET_CONTENT_WRAPPERS, contentWrappers }
}
export function getContentWrappersFailed(error){
  return { type: GET_CONTENT_WRAPPERS_FAILED, error }
}

export function getContentWrapperAsync(contentWrapper){
  return dispatch => {
    return fetch(host + `/contentwrappers/${contentWrapper.id}`)
    .then(response => {
      if (response.ok) {
        response.json()
        .then(json => {
          dispatch(setActiveContentWrapper(json)); 
          dispatch(getContentWrapper(json));

        });
        return new Promise((resolve, reject) => { resolve() });
      } else {
        response.text()
        .then((json) => {throw JSON.parse(json).error})
        .catch((error) => dispatch(getContentWrapperFailed(error)));
        return new Promise((resolve, reject) => { reject() });
      }
    })
  }
}
export function getContentWrapper(contentWrapper){
  return { type: GET_CONTENT_WRAPPER, contentWrapper }
}
export function getContentWrapperFailed(error){
  return { type: GET_CONTENT_WRAPPER_FAILED, error }
}
