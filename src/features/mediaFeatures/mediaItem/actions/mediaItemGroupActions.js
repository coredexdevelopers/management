import { SET_ACTIVE_MEDIA_ITEM_GROUP } from './types';

const host = process.env.REACT_APP_MAIN_HOST;

export function setActiveMediaItemGroup(activeMediaItemGroup){
  return { type: SET_ACTIVE_MEDIA_ITEM_GROUP, activeMediaItemGroup }
}
