import React, {Component} from "react";
import { connect } from 'react-redux';
import { push, goBack } from 'react-router-redux';
import './index.css';
import ContentWrapperBox from '../../components/ContentWrapperBox';
import AddContentWrapperBox from '../../components/AddContentWrapperBox';
import * as contentWrapperGetActions from '../../actions/contentWrapperGetActions';
import * as contentWrapperActions from '../../actions/contentWrapperActions';
import * as contentWrapperDeleteActions from '../../actions/contentWrapperDeleteActions';
import { FilePond } from 'react-filepond';
import { Grid, Menu, Sticky } from 'semantic-ui-react';

const filepondUrl = process.env.REACT_APP_FILEPOND_HOST;
class ContentWrappersScreen extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount(){
    this.props.getContentWrappersAsync();
  }

  handleContentWrapperBoxClick(contentWrapper){
    //this.props.setActiveContentWrapper(contentWrapper);
  }

  handleContentWrapperOpen(contentWrapper){
    this.props.push(`/mediaItems/contentWrapper/${contentWrapper.id}`);
  }
  handleContentWrapperEdit(contentWrapper){
    this.props.push(`/editContentWrapper/${contentWrapper.id}`);
  }
  handleContentWrapperDelete(contentWrapper){
    const errors = this.props.errors;
    this.props.deleteContentWrapperAsync(null, contentWrapper)
    .then(() => {
      alert('تم حذف هذه المجموعة بنجاح');
      this.props.getContentWrappersAsync();
    }).catch(() => {
      alert('فشلت عملية الحذف');
    });
  }

  render() {
    const contentWrappers = this.props.contentWrappers;
    const errors = this.props.errors;
    const push = this.props.push;
    return (
      <div className="app-container">
        <Sticky>
          <Menu>
            <Menu.Item header>
              الشيخ الدكتور سالم الرافعي
            </Menu.Item>
            <Menu.Item>
              المجموعات الرئيسية
            </Menu.Item>
          </Menu>
        </Sticky>
        <Grid>
          <Grid.Column mobile={16} tablet={6} computer={4}>
            <AddContentWrapperBox onClick={() => push('/addContentWrapper')} />
          </Grid.Column>
          {contentWrappers.map((contentWrapper) =>
            <Grid.Column mobile={16} tablet={6} computer={4} centered>
              <ContentWrapperBox contentWrapper={contentWrapper}
                onClick={() => this.handleContentWrapperBoxClick(contentWrapper)}
                showControls={true}
                onOpen={() => this.handleContentWrapperOpen(contentWrapper)}
                onEdit={() => this.handleContentWrapperEdit(contentWrapper)}
                onDelete={() => this.handleContentWrapperDelete(contentWrapper)}
              />
          </Grid.Column>
          )}
          <Grid.Column mobile={16} tablet={6} computer={4}>
            <AddContentWrapperBox onClick={() => push('/addContentWrapper')} />
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      contentWrappers: state.contentWrapper.contentWrappers,
      activeContentWrapper: state.contentWrapper.activeContentWrapper,
      errors: state.contentWrapper.errors
    };
};

const mapDispatchToProps = {
  ...contentWrapperGetActions,
  ...contentWrapperActions,
  ...contentWrapperDeleteActions,
  push,
  goBack
}

export default connect(mapStateToProps, mapDispatchToProps)(ContentWrappersScreen);
