import React from 'react';
import './index.css';
import { Card, Button, Input, Grid } from 'semantic-ui-react';
const AddMediaItemGroupForm = ({...props}) => {
  return (
    <Card {...props} fluid>
      <Card.Content>
        <Card.Header>
          <Input type="text" onInput={(e) => props.onNewTitle(e.target.value)}
            placeholder="العنوان"
            value={props.newTitle}
            />
        </Card.Header>
      </Card.Content>
      <Card.Content>
        <Grid>
          <Grid.Column computer={16} tablet={16} mobile={16}>
            <Input fluid label="التاريخ" type="date" onInput={(e) => props.onNewRecordedAt(e.target.value)}
              value={props.newDescription}
              />
          </Grid.Column>
        </Grid>
      </Card.Content>
      <Card.Content extra>
        <Button className="margin-bottom-gap" fluid primary onClick={() => props.onSave()}>Save</Button>
        <Button className="margin-bottom-gap" fluid secondary  onClick={() => props.onCancel()}>Cancel</Button>
      </Card.Content>
    </Card>
  );
};


export default AddMediaItemGroupForm;
