import { SEND_PUSH_NOTIFICATIONS, SEND_PUSH_NOTIFICATIONS_FAILED } from './types';

const host = process.env.REACT_APP_MAIN_HOST;



export function sendPushNotificationsAsync(token, mediaItem){
  const myHeaders = new Headers();
  myHeaders.append('Authorization',"Bearer " + token);
  var body = `جديد: ${mediaItem.title}`;
  var formData = new FormData();
  formData.append('body', body); // the message of the notification
  formData.append('data', JSON.stringify({'mediaItem': mediaItem})); // the data contained in the notification
  return dispatch => {
    return fetch(host+'/pushtokens/sendToAll',
      {
        method: 'post',
        body: formData,
        headers: myHeaders
      }
    )
    .then(response => {
      if (response.ok) {
        response.json()
        .then(json => {dispatch(sendPushNotifications())});
        return new Promise((resolve, reject) => { resolve() });
      } else {
        response.text()
        .then((json) => {throw JSON.parse(json).error})
        .catch((error) => dispatch(sendPushNotificationsFailed(error)));
        return new Promise((resolve, reject) => { reject() });

      }
    })
  }
}

export function sendPushNotifications(){
  return { type: SEND_PUSH_NOTIFICATIONS }
}
export function sendPushNotificationsFailed(error){
  return { type: SEND_PUSH_NOTIFICATIONS_FAILED, error }
}
