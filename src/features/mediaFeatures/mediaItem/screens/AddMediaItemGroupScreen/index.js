import React, {Component} from "react";
import { connect } from 'react-redux';
import { push, goBack } from 'react-router-redux';
import './index.css';
import AddMediaItemGroupForm from '../../components/AddMediaItemGroupForm';
import * as mediaItemActions from '../../actions/mediaItemActions';
import * as mediaItemAddActions from '../../actions/mediaItemAddActions';
import { Message, Header, Container, Sticky, Menu, Grid, Button } from 'semantic-ui-react';

class AddMediaItemGroupScreen extends Component {
  constructor(props) {
    super(props);
    this.state = { saved: 0 };
  }

  componentDidMount(){
    this.resetValues();
  }

  resetValues(){
    this.props.setNewTitle(null);
    this.props.setNewRecordedAt(null);
  }

  handleAddMediaItemAsync(){
    const addMediaItemAsync = this.props.addMediaItemAsync;
    var parentContentWrapper = null;
    var parentMediaItemGroup = null;
    if(this.props.match.params.parentEntity === 'contentWrapper'){
      parentContentWrapper = {id: this.props.match.params.parentId};
      parentMediaItemGroup = null;
    } else if(this.props.match.params.parentEntity === 'mediaItemGroup'){
      parentMediaItemGroup = {id: this.props.match.params.parentId};
      parentContentWrapper = null;
    }
    addMediaItemAsync(this.props.token, null, null, null, null, this.props.newTitle,
      null, null, 1, this.props.newRecordedAt, parentContentWrapper, parentMediaItemGroup)
    .then(() => {
      this.setState({saved: this.state.saved+1});
    })
  }



  render() {
    return (
      <div className="app-container">
        <Sticky>
          <Menu>
            <Menu.Item icon="arrow left" onClick={() => this.props.goBack()}>
            </Menu.Item>
            <Menu.Item>
              إضافة مجموعة
            </Menu.Item>
          </Menu>
        </Sticky>
        {this.props.errors.length > 0 &&
          <Message negative>{this.props.errors.length + ' errors occured.'}</Message>
        }
        {this.state.saved > 0 &&
          <Message positive>{this.state.saved + ' saved successfully.'}</Message>
        }
        <Grid>
          <Grid.Column computer={8}>
            <AddMediaItemGroupForm
              onNewTitle={(newTitle) => this.props.setNewTitle(newTitle)}
              newTitle={this.props.newTitle}
              onNewRecordedAt={(newRecordedAt) => this.props.setNewRecordedAt(newRecordedAt)}
              newRecordedAt={this.props.newRecordedAt}
              onSave={() => this.handleAddMediaItemAsync()}
              onCancel={() => this.props.goBack()}
            />
          </Grid.Column>
        </Grid>

      </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      token: null,
      newTitle: state.mediaItem.newTitle,
      newRecordedAt: state.mediaItem.newRecordedAt,
      errors: state.mediaItem.errors
    };
};
const mapDispatchToProps = {
  ...mediaItemAddActions,
  ...mediaItemActions,
  push,
  goBack
}

export default connect(mapStateToProps, mapDispatchToProps)(AddMediaItemGroupScreen);
