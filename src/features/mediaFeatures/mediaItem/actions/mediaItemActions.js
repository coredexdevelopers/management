import { SET_ACTIVE_MEDIA_ITEM, SET_MEDIA_ITEM_FILTER, SET_NEW_TITLE,
  SET_NEW_DESCRIPTION,
  SET_NEW_THUMBNAIL,
  SET_NEW_LOW_QUALITY_AUDIO,
  SET_NEW_HIGH_QUALITY_AUDIO,
  SET_NEW_VIDEO,
  SET_NEW_PDF,
  SET_NEW_RECORDED_AT  } from './types';

const host = process.env.REACT_APP_MAIN_HOST;

export function setActiveMediaItem(activeMediaItem){
  return { type: SET_ACTIVE_MEDIA_ITEM, activeMediaItem }
}

export function setMediaItemFilter(mediaItemFilter){
  return { type: SET_MEDIA_ITEM_FILTER, mediaItemFilter }
}

export function setNewTitle(newTitle){
  return { type: SET_NEW_TITLE, newTitle }
}
export function setNewDescription(newDescription){
  return { type: SET_NEW_DESCRIPTION, newDescription }
}
export function setNewThumbnail(newThumbnail){
  return { type: SET_NEW_THUMBNAIL, newThumbnail }
}
export function setNewLowQualityAudio(newLowQualityAudio){
  return { type: SET_NEW_LOW_QUALITY_AUDIO, newLowQualityAudio }
}
export function setNewHighQualityAudio(newHighQualityAudio){
  return { type: SET_NEW_HIGH_QUALITY_AUDIO, newHighQualityAudio }
}
export function setNewVideo(newVideo){
  return { type: SET_NEW_VIDEO, newVideo }
}
export function setNewPdf(newPdf){
  return { type: SET_NEW_PDF, newPdf }
}
export function setNewRecordedAt(newRecordedAt){
  return { type: SET_NEW_RECORDED_AT, newRecordedAt }
}
