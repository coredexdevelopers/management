import React from 'react';
import './index.css';
import { Card, Button } from 'semantic-ui-react';

const ContentWrapperBox = ({contentWrapper, ...props}) => {
  return (
      <Card {...props} fluid>
        <Card.Content>
          <Card.Header>
            {contentWrapper.title}
          </Card.Header>
        </Card.Content>
        <Card.Content extra>
          {
            props.showControls &&
            <React.Fragment>
              <Button className='content-wrapper-box-button' fluid primary onClick={() => props.onOpen()}>فتح</Button>
              <Button className='content-wrapper-box-button' fluid color="violet" onClick={() => props.onEdit()}>تعديل</Button>
              <Button className='content-wrapper-box-button' fluid color="black" onClick={() => props.onDelete()}>حذف</Button>
            </React.Fragment>
          }
        </Card.Content>
      </Card>
  );
};


export default ContentWrapperBox;
