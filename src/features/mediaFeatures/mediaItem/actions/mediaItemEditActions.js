import { EDIT_MEDIA_ITEM, EDIT_MEDIA_ITEM_FAILED } from './types';

const host = process.env.REACT_APP_MAIN_HOST;

export function editMediaItemAsync(token, mediaItem, newLowQualityAudio, newHighQualityAudio,
newVideo, newPdf, newTitle, newDescription, newThumbnail, newIsGroup, newRecordedAt, parentContentWrapper, parentMediaItemGroup){
  const myHeaders = new Headers();
  myHeaders.append('Authorization',"Bearer " + token);

  var formData = new FormData();
  formData.append('lowQualityAudio', newLowQualityAudio);
  formData.append('highQualityAudio', newHighQualityAudio)
  formData.append('video', newVideo);
  formData.append('pdf', newPdf);
  formData.append('title', newTitle);
  formData.append('description', newDescription);
  formData.append('thumbnail', newThumbnail);
  formData.append('isGroup', newIsGroup);
  if(newRecordedAt){
    formData.append('recordedAt', newRecordedAt);
  }
  if(parentContentWrapper){
    formData.append('contentWrapperId', parentContentWrapper?parentContentWrapper.id:0);
  }
  if(parentMediaItemGroup){
    formData.append('groupMediaItemId', parentMediaItemGroup?parentMediaItemGroup.id:0);
  }


  formData.append('_method', 'PUT');
  return dispatch => {
    return fetch(host+`/mediaitems/${mediaItem.id}`,
      {
        method: 'post',
        body: formData,
        headers: myHeaders
      }
    )
    .then(response => {
      if (response.ok) {
        response.json()
        .then(json => {dispatch(editMediaItem(json))});
        return new Promise((resolve, reject) => { resolve() });
      } else {
        response.text()
        .then((json) => {throw JSON.parse(json).error})
        .catch((error) => dispatch(editMediaItemFailed(error)));
        return new Promise((resolve, reject) => { reject() });
      }
    })
  }
}

export function editMediaItem(mediaItem){
  return { type: EDIT_MEDIA_ITEM, mediaItem }
}
export function editMediaItemFailed(error){
  return { type: EDIT_MEDIA_ITEM_FAILED, error }
}
