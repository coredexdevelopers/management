import {
  SET_DIRECTORY_PATH,
  GET_FILES,
  GET_FILES_FAILED,
  ADD_DIRECTORY,
  ADD_DIRECTORY_FAILED
}
 from '../actions/types';

const initialState = {
  files: [],
  directoryPath: '/',
  errors: []
}

const fileBrowserReducer = function(state = initialState, action) {
  switch (action.type) {
    case SET_DIRECTORY_PATH:
      return {
        ...state,
        directoryPath: action.directoryPath
      }
    case GET_FILES:
      return {
        ...state,
        files: action.files,
        errors: []
      }
    case GET_FILES_FAILED:
      return {
        ...state,
        errors: [...state.errors, action.error]
      }
    case ADD_DIRECTORY:
      return {
        ...state,
        errors: []
      }
    case ADD_DIRECTORY_FAILED:
      return {
        ...state,
        errors: [...state.errors, action.error]
      }
    default:
      return state;
  }
};

export default fileBrowserReducer;
