import { SET_ACTIVE_MEDIA_ITEM_GROUP } from '../actions/types';

const initialState = {
  activeMediaItemGroup: null
}

  const mediaItemGroupReducer = function(state = initialState, action) {
    switch (action.type) {
      case SET_ACTIVE_MEDIA_ITEM_GROUP:
        return {
          ...state,
          activeMediaItemGroup: action.activeMediaItemGroup
        }
      default:
        return state;
    }
  };

  export default mediaItemGroupReducer;
