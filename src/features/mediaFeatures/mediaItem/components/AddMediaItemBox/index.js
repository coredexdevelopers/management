import React from 'react';
import './index.css';
import { Card, Button, Icon } from 'semantic-ui-react';
const AddMediaItemBox = ({...props}) => {
  return (
      <Card {...props} fluid>
        <Card.Content>
          <Button fluid color="green">
            إضافة محتوى
          </Button>
        </Card.Content>
      </Card>
  );
};


export default AddMediaItemBox;
