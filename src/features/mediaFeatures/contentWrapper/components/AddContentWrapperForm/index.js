import React from 'react';
import './index.css';
import { Card, Input, Button } from 'semantic-ui-react';

const AddContentWrapperForm = ({...props}) => {
  return (
      <Card {...props}>
        <Card.Content>
          <Card.Header>
            <Input type="text" onInput={(e) => props.onNewTitle(e.target.value)}
              placeholder="العنوان"
              value={props.newTitle}
              />
          </Card.Header>
        </Card.Content>
        <Card.Content extra>
          <Button className="margin-bottom-gap" fluid primary onClick={() => props.onSave()}>Save</Button>
          <Button className="margin-bottom-gap" fluid secondary  onClick={() => props.onCancel()}>Cancel</Button>
        </Card.Content>
      </Card>
  );
};


export default AddContentWrapperForm;
