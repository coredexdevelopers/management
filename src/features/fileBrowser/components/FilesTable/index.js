import React from 'react';
import './index.css';
import { Table, Button } from 'semantic-ui-react';
import { format } from 'date-fns';
const arLocale = require('date-fns/locale/ar');

const FilesTable = ({files, ...props}) => {
  if(files === null){
    return (<div>No files array.</div>)
  }
  return (
      <Table {...props} fluid>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>اسم الملف</Table.HeaderCell>
            <Table.HeaderCell>تاريخ الرفع</Table.HeaderCell>
            <Table.HeaderCell>نسخ</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          <Table.Row>
            <Table.Cell>
              <Button onClick={() => props.onBackClick()}>عودة إلى المجلد السابق</Button>
              <Button color="green" onClick={() => props.onNewFolderClick()}>إضافة مجلد جديد</Button>
            </Table.Cell>
            <Table.Cell />
            <Table.Cell />
          </Table.Row>
          {files.map((file) => {
            return(
              <Table.Row>
                <Table.Cell>
                  {file.isDirectory ?
                    <Button onClick={(e) => props.onDirectoryClick(file)}>{file.name}</Button> :
                      file.name
                  }
                </Table.Cell>
                <Table.Cell>{format(file.date, 'dddd D MMMM YYYY hh:mm a', {locale: arLocale})}</Table.Cell>
                <Table.Cell><input onClick={(e) => props.onCopyClick(file, e.target)} value={file.url} /></Table.Cell>
              </Table.Row>
            )
          })}
        </Table.Body>
      </Table>
  );
};


export default FilesTable;
