import { GET_MEDIA_ITEMS, GET_MEDIA_ITEMS_FAILED, GET_MEDIA_ITEM, GET_MEDIA_ITEM_FAILED } from './types';
import { setActiveMediaItem, setMediaItemFilter } from './mediaItemActions';
import { setActiveMediaItemGroup } from './mediaItemGroupActions';
import { setActiveContentWrapper } from '../../contentWrapper/actions/contentWrapperActions';

const host = process.env.REACT_APP_MAIN_HOST;


export function getMediaItemsAsync(mediaItemFilter){
  return dispatch => {
    dispatch(setActiveMediaItemGroup({id: mediaItemFilter.groupMediaItemId}));
    dispatch(setActiveContentWrapper({id: mediaItemFilter.contentWrapperId}));
    dispatch(setMediaItemFilter(mediaItemFilter));
    return fetch(host + `/mediaitems?mediaItemFilter=${JSON.stringify(mediaItemFilter)}`)
    .then(response => {
      if (response.ok) {
        response.json()
        .then(json => {dispatch(getMediaItems(json))});
        return new Promise((resolve, reject) => { resolve() });
      } else {
        response.text()
        .then((json) => {throw JSON.parse(json).error})
        .catch((error) => dispatch(getMediaItemsFailed(error)));
        return new Promise((resolve, reject) => { reject() });

      }
    })
  }
}
export function getMediaItems(mediaItems){
  return { type: GET_MEDIA_ITEMS, mediaItems }
}
export function getMediaItemsFailed(error){
  return { type: GET_MEDIA_ITEMS_FAILED, error }
}

export function getMediaItemAsync(mediaItem){
  return dispatch => {
    return fetch(host + `/mediaitems/${mediaItem.id}`)
    .then(response => {
      if (response.ok) {
        response.json()
        .then(json => {
          dispatch(getMediaItem(json));
          if(json.isGroup){
            dispatch(setActiveMediaItemGroup(json));
          } else {
            dispatch(setActiveMediaItem(json));
          }

        });
        return new Promise((resolve, reject) => { resolve() });
      } else {
        response.text()
        .then((json) => {throw JSON.parse(json).error})
        .catch((error) => dispatch(getMediaItemFailed(error)));
        return new Promise((resolve, reject) => { reject() });

      }
    })
  }
}
export function getMediaItem(activeMediaItem){
  return { type: GET_MEDIA_ITEM, activeMediaItem }
}
export function getMediaItemFailed(error){
  return { type: GET_MEDIA_ITEM_FAILED, error }
}
