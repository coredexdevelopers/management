import React from 'react';
import './index.css';
import { Card, Button, Icon } from 'semantic-ui-react';
const AddMediaItemGroupBox = ({...props}) => {
  return (
    <Card {...props} fluid>
      <Card.Content>
        <Button fluid color="green">
          إضافة مجموعة محتويات
        </Button>
      </Card.Content>
    </Card>
  );
};


export default AddMediaItemGroupBox;
