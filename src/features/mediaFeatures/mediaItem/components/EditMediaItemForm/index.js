import React from 'react';
import { Card, Input, Grid, Button } from 'semantic-ui-react';
import './index.css';
import { format } from 'date-fns';

const EditMediaItemForm = ({mediaItem, ...props}) => {
  if(!mediaItem){
    return("No media item to edit.");
  }
  return (
      <Card {...props} fluid>
        <Card.Content>
          <Card.Header>
            <Input type="text" onInput={(e) => props.onNewTitle(e.target.value)}
              placeholder="العنوان"
              value={props.newTitle}
              />
          </Card.Header>
        </Card.Content>
        <Card.Content>
          <Grid>
            <Grid.Column computer={5} tablet={6} mobile={16}>
              <Input fluid label="الوصف" type="text" onInput={(e) => props.onNewDescription(e.target.value)}
                value={props.newDescription}
                />
            </Grid.Column>
            <Grid.Column computer={5} tablet={6} mobile={16}>
              <Input fluid label="الصورة" type="text" onInput={(e) => props.onNewThumbnail(e.target.value)}
                value={props.newThumbnail}
                />
            </Grid.Column>
            <Grid.Column computer={5} tablet={6} mobile={16}>
              <Input fluid label="تاريخ التسجيل" type="date" onInput={(e) => props.onNewRecordedAt(e.target.value)}
                value={format(new Date(props.newRecordedAt), 'YYYY-MM-DD')}
                />
            </Grid.Column>
            <Grid.Column computer={5} tablet={6} mobile={16}>
              <Input fluid label="صوت عادي الجودة" type="text" onInput={(e) => props.onNewLowQualityAudio(e.target.value)}
                value={props.newLowQualityAudio}
                />
            </Grid.Column>
            <Grid.Column computer={5} tablet={6} mobile={16}>
              <Input fluid label="صوت عالي الجودة" type="text" onInput={(e) => props.onNewHighQualityAudio(e.target.value)}
                value={props.newHighQualityAudio}
                />
            </Grid.Column>
            <Grid.Column computer={5} tablet={6} mobile={16}>
              <Input fluid label="فيديو" type="text" onInput={(e) => props.onNewVideo(e.target.value)}
                value={props.newVideo}
                />
            </Grid.Column>
            <Grid.Column computer={5} tablet={6} mobile={16}>
              <Input fluid label="PDF" type="text" onInput={(e) => props.onNewPdf(e.target.value)}
                    value={props.newPdf}
                />
            </Grid.Column>
          </Grid>

        </Card.Content>
        <Grid>
          <Grid.Column computer={16}>
            <Button fluid color="blue" onClick={() => props.onSave()}>Save</Button>
          </Grid.Column>
          <Grid.Column computer={16}>
            <Button fluid color="grey" onClick={() => props.onCancel()}>Cancel</Button>
          </Grid.Column>
        </Grid>

      </Card>
  );
};


export default EditMediaItemForm;
