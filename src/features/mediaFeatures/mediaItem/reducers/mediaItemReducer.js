import MediaItemFilter from './standardizers/MediaItemFilter';
import {
  GET_MEDIA_ITEMS,
  GET_MEDIA_ITEMS_FAILED,
  GET_MEDIA_ITEM,
  GET_MEDIA_ITEM_FAILED,
  EDIT_MEDIA_ITEM,
  EDIT_MEDIA_ITEM_FAILED,
  ADD_MEDIA_ITEM,
  ADD_MEDIA_ITEM_FAILED,
  DELETE_MEDIA_ITEM,
  DELETE_MEDIA_ITEM_FAILED,
  SET_ACTIVE_MEDIA_ITEM,
  SET_ACTIVE_MEDIA_ITEM_GROUP,
  SET_MEDIA_ITEM_FILTER,
  SET_NEW_TITLE,
  SET_NEW_VIDEO,
  SET_NEW_HIGH_QUALITY_AUDIO,
  SET_NEW_LOW_QUALITY_AUDIO,
  SET_NEW_PDF,
  SET_NEW_DESCRIPTION,
  SET_NEW_THUMBNAIL,
  SET_NEW_RECORDED_AT
}
 from '../actions/types';

const initialState = {
  mediaItems: [],
  activeMediaItem: null,
  newLowQualityAudio: '',
  newHighQualityAudio: '',
  newVideo: '',
  newPdf: '',
  newTitle: '',
  newDescription: '',
  newThumbnail: '',
  newIsGroup: false,
  newRecordedAt: null,
  mediaItemFilter: new MediaItemFilter(),
  errors: [],
  isLoading: false,
}

const mediaItemReducer = function(state = initialState, action) {
  switch (action.type) {
    case GET_MEDIA_ITEMS:
      return {
        ...state,
        mediaItems: action.mediaItems,
        errors: []
      }
    case GET_MEDIA_ITEMS_FAILED:
      return {
        ...state,
        errors: [...state.errors, action.error]
      }
    case EDIT_MEDIA_ITEM_FAILED:
      return {
        ...state,
        errors: [...state.errors, action.error]
      }
    case ADD_MEDIA_ITEM:
      return {
        ...state,
        errors: []
      }
    case ADD_MEDIA_ITEM_FAILED:
      return {
        ...state,
        errors: [...state.errors, action.error]
      }
    case DELETE_MEDIA_ITEM:
      return {
        ...state,
        errors: []
      }
    case DELETE_MEDIA_ITEM_FAILED:
      return {
        ...state,
        errors: [...state.errors, action.error]
      }
    case SET_ACTIVE_MEDIA_ITEM:
      return {
        ...state,
        activeMediaItem: action.activeMediaItem
      }
    case SET_MEDIA_ITEM_FILTER:
      return {
        ...state,
        mediaItemFilter: action.mediaItemFilter
      }
    case SET_NEW_TITLE:
      return {
        ...state,
        newTitle: action.newTitle
      }
    case SET_NEW_DESCRIPTION:
      return {
        ...state,
        newDescription: action.newDescription
      }
    case SET_NEW_THUMBNAIL:
      return {
        ...state,
        newThumbnail: action.newThumbnail
      }
    case SET_NEW_VIDEO:
      return {
        ...state,
        newVideo: action.newVideo
      }
    case SET_NEW_LOW_QUALITY_AUDIO:
      return {
        ...state,
        newLowQualityAudio: action.newLowQualityAudio
      }
    case SET_NEW_HIGH_QUALITY_AUDIO:
      return {
        ...state,
        newHighQualityAudio: action.newHighQualityAudio
      }
    case SET_NEW_PDF:
      return {
        ...state,
        newPdf: action.newPdf
      }
    case SET_NEW_RECORDED_AT:
      return {
        ...state,
        newRecordedAt: action.newRecordedAt
      }

    default:
      return state;
  }
};

export default mediaItemReducer;
