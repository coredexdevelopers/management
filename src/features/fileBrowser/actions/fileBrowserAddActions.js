import { ADD_DIRECTORY, ADD_DIRECTORY_FAILED } from './types';

const host = process.env.REACT_APP_FILEPOND_HOST;

export function addDirectoryAsync(token, directoryPath){
  const myHeaders = new Headers();
  myHeaders.append('Authorization',"Bearer " + token);
  var formData = new FormData();
  formData.append('directoryPath', directoryPath);

  return dispatch => {
    return fetch(host+'/createDirectory.php',
      {
        method: 'POST',
        body: formData,
        headers: myHeaders
      }
    )
    .then(response => {
      if (response.ok) {
        response.json()
        .then(json => {dispatch(addDirectory(json))});
        return new Promise((resolve, reject) => { resolve() });
      } else {
        response.text()
        .then((json) => {throw JSON.parse(json).error})
        .catch((error) => dispatch(addDirectoryFailed(error)));
        return new Promise((resolve, reject) => { reject() });

      }
    })
  }
}

export function addDirectory(files){
  return { type: ADD_DIRECTORY, files }
}
export function addDirectoryFailed(error){
  return { type: ADD_DIRECTORY_FAILED, error }
}
