import { ADD_CONTENT_WRAPPER, ADD_CONTENT_WRAPPER_FAILED } from './types';

const host = process.env.REACT_APP_MAIN_HOST;

export function addContentWrapperAsync(token, newTitle, newDescription){
  const myHeaders = new Headers();
  myHeaders.append('Authorization',"Bearer " + token);

  var formData = new FormData();
  formData.append('title', newTitle);
  formData.append('description', newDescription);

  return dispatch => {
    return fetch(host+'/contentwrappers',
      {
        method: 'post',
        body: formData,
        headers: myHeaders
      }
    )
    .then(response => {
      if (response.ok) {
        response.json()
        .then(json => {dispatch(addContentWrapper(json))});
        return new Promise((resolve, reject) => { resolve() });
      } else {
        response.text()
        .then((json) => {throw JSON.parse(json).error})
        .catch((error) => dispatch(addContentWrapperFailed(error)));
        return new Promise((resolve, reject) => { reject() });
      }
    })
  }
}

export function addContentWrapper(contentWrapper){
  return { type: ADD_CONTENT_WRAPPER, contentWrapper }
}
export function addContentWrapperFailed(error){
  return { type: ADD_CONTENT_WRAPPER_FAILED, error }
}
