import { GET_FILES, GET_FILES_FAILED } from './types';

const host = process.env.REACT_APP_FILEPOND_HOST;

export function getFilesAsync(directoryPath){
  return dispatch => {
    return fetch(host + `/getFiles.php?directoryPath=${directoryPath}`)
    .then(response => {
      if (response.ok) {
        response.json()
        .then(json => {dispatch(getFiles(json))});
        return new Promise((resolve, reject) => { resolve() });
      } else {
        response.text()
        .then((json) => {throw JSON.parse(json).error})
        .catch((error) => dispatch(getFilesFailed(error)));
        return new Promise((resolve, reject) => { reject() });

      }
    })
  }
}
export function getFiles(files){
  return { type: GET_FILES, files }
}
export function getFilesFailed(error){
  return { type: GET_FILES_FAILED, error }
}
