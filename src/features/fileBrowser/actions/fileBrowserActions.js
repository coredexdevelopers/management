import { SET_DIRECTORY_PATH } from './types';

const host = process.env.REACT_APP_FILEPOND_HOST;

export function setDirectoryPath(directoryPath){
  return { type: SET_DIRECTORY_PATH, directoryPath }
}
