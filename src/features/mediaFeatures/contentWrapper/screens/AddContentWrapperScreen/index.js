import React, {Component} from "react";
import { connect } from 'react-redux';
import { push, goBack } from 'react-router-redux';
import './index.css';
import AddContentWrapperForm from '../../components/AddContentWrapperForm';
import * as contentWrapperActions from '../../actions/contentWrapperActions';
import * as contentWrapperAddActions from '../../actions/contentWrapperAddActions';
import { Message, Header, Container, Sticky, Menu, Grid, Button } from 'semantic-ui-react';
import { FilePond } from 'react-filepond';

const filepondUrl = process.env.REACT_APP_FILEPOND_HOST;
class AddContentWrapperScreen extends Component {
  constructor(props) {
    super(props);
    this.state = { saved: 0 };
  }

  componentDidMount(){
    this.resetValues();
  }

  resetValues() {
    this.props.setNewTitle('');
  }

  handleAddContentWrapperAsync(){
    const addContentWrapperAsync = this.props.addContentWrapperAsync;
    addContentWrapperAsync(this.props.token, this.props.newTitle, null)
    .then(() => {
      this.setState({saved: this.state.saved+1});
    })
  }

  render() {
    const setNewTitle = this.props.setNewTitle;
    const newTitle = this.props.newTitle;
    const goBack = this.props.goBack;
    return (
      <div className="app-container">
        <Sticky>
          <Menu>
            <Menu.Item icon="arrow left" onClick={() => this.props.goBack()}>
            </Menu.Item>
            <Menu.Item>
              إضافة مجموعة رئيسيّة
            </Menu.Item>
          </Menu>
        </Sticky>
        {this.state.saved > 0 &&
          <Message positive>
            {this.state.saved} saved successfully.
          </Message>
        }

        <Grid centered>
          <Grid.Column>
            <AddContentWrapperForm
              fluid
              onNewTitle={(newTitle) => setNewTitle(newTitle)}
              newTitle={newTitle}
              onSave={() => this.handleAddContentWrapperAsync()}
              onCancel={() => goBack()}
              />
          </Grid.Column>
        </Grid>
        <div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      newTitle: state.contentWrapper.newTitle
    };
};
const mapDispatchToProps = {
  ...contentWrapperActions,
  ...contentWrapperAddActions,
  push,
  goBack,

}
// const mapDispatchToProps = (dispatch) => {
//     return {
//       push: (page) => dispatch(push(page)),
//       goBack: () => dispatch(goBack())
//     };
// };

export default connect(mapStateToProps, mapDispatchToProps)(AddContentWrapperScreen);
