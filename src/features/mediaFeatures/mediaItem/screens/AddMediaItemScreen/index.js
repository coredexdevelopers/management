import React, {Component} from "react";
import { connect } from 'react-redux';
import { push, goBack } from 'react-router-redux';
import './index.css';
import AddMediaItemForm from '../../components/AddMediaItemForm';
import * as mediaItemActions from '../../actions/mediaItemActions';
import * as mediaItemGroupActions from '../../actions/mediaItemGroupActions';
import * as mediaItemAddActions from '../../actions/mediaItemAddActions';
import * as mediaItemGetActions from '../../actions/mediaItemGetActions';
import * as contentWrapperGetActions from '../../../contentWrapper/actions/contentWrapperGetActions';
import { setActiveContentWrapper } from '../../../contentWrapper/actions/contentWrapperActions';
import { Message, Header, Container, Sticky, Menu, Grid, Button } from 'semantic-ui-react';
import { FilePond } from 'react-filepond';
const filepondUrl = process.env.REACT_APP_FILEPOND_HOST;
const webFtpClientUrl = process.env.REACT_APP_WEB_FTP_CLIENT_HOST;

class AddMediaItemScreen extends Component {
  constructor(props) {
    super(props);
    this.state = { saved: 0 };
  }

  componentDidMount(){
    const previousLocation = this.props.previousLocation;
    if(previousLocation === null || previousLocation === '/fileBrowser') {
      // if coming back from the file browser, we don't want to delete the content
      this.getParentEntity();
      return;
    }
    this.resetValues();
    this.getParentEntity();

  }

  getParentEntity(){
    const parentEntity = this.props.match.params.parentEntity;
    const parentId = this.props.match.params.parentId;
    if(parentId){
      if(parentEntity === 'contentWrapper'){
        this.props.getContentWrapperAsync({id: parentId}); // sets the active content wrapper
      } else if(parentEntity === 'mediaItemGroup'){
        this.props.getMediaItemAsync({id: parentId}); // sets the active media item (group)
      }
    }
  }

  resetValues(){
    this.props.setNewLowQualityAudio('');
    this.props.setNewHighQualityAudio('');
    this.props.setNewVideo('');
    this.props.setNewPdf('');
    this.props.setNewTitle('');
    this.props.setNewDescription('');
    this.props.setNewRecordedAt('');
    this.props.setNewThumbnail('');
    this.props.setActiveContentWrapper(null);
    this.props.setActiveMediaItemGroup(null);

  }

  handleAddMediaItemAsync(){
    const addMediaItemAsync = this.props.addMediaItemAsync;
    addMediaItemAsync(this.props.token, this.props.newLowQualityAudio, this.props.newHighQualityAudio,
      this.props.newVideo, this.props.newPdf, this.props.newTitle,
      this.props.newDescription, this.props.newThumbnail, 0, this.props.newRecordedAt, this.props.activeContentWrapper,
      this.props.activeMediaItemGroup)
    .then(() => {
      this.setState({saved: this.state.saved+1});
      this.resetValues();
      this.getParentEntity();
    })
  }

  getYoutubeVideoId(url) {
      var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
      var match = url.match(regExp);

      if (match && match[2].length == 11) {
          return match[2];
      } else {
          return 'error';
      }
  }


  handleSetNewVideo(newVideo){
    if(newVideo.includes('youtube') || newVideo.includes('youtu.be')){
      newVideo = "https://youtube.com/embed/"+this.getYoutubeVideoId(newVideo);
    }
    this.props.setNewVideo(newVideo);
  }


  render() {
    return (
      <div className="app-container">
        <Sticky>
          <Menu>
            <Menu.Item icon="arrow left" onClick={() => this.props.goBack()}>
            </Menu.Item>
            <Menu.Item>
              إضافة محتوى
            </Menu.Item>
            <Menu.Item>
              <Button onClick={() => this.props.push('/fileBrowser')}>الملفات - بسيط</Button>
            </Menu.Item>
            <Menu.Item>
              <Button onClick={() => window.open(webFtpClientUrl, "_blank")}>الملفات - متطور</Button>
            </Menu.Item>
          </Menu>
        </Sticky>
        {
          this.props.errors.length > 0 &&
          <Message negative>{this.props.errors.length + ' errors occured.'}</Message>
        }
        {
          this.state.saved > 0 &&
          <Message positive>{this.state.saved + ' saved successfully.'}</Message>
        }
      <AddMediaItemForm
        onNewLowQualityAudio={(newLowQualityAudio) => this.props.setNewLowQualityAudio(newLowQualityAudio)}
        newLowQualityAudio={this.props.newLowQualityAudio}
        onNewHighQualityAudio={(newHighQualityAudio) => this.props.setNewHighQualityAudio(newHighQualityAudio)}
        newHighQualityAudio={this.props.newHighQualityAudio}
        onNewVideo={(newVideo) => this.handleSetNewVideo(newVideo)}
        newVideo={this.props.newVideo}
        onNewPdf={(newPdf) => this.props.setNewPdf(newPdf)}
        newPdf={this.props.newPdf}
        onNewTitle={(newTitle) => this.props.setNewTitle(newTitle)}
        newTitle={this.props.newTitle}
        onNewDescription={(newDescription) => this.props.setNewDescription(newDescription)}
        onNewThumbnail={(newThumbnail) => this.props.setNewThumbnail(newThumbnail)}
        newDescription={this.props.newDescription}
        onNewRecordedAt={(newRecodedAt) => this.props.setNewRecordedAt(newRecodedAt)}
        newRecordedAt={this.props.newRecordedAt}

        onSave={() => this.handleAddMediaItemAsync()}
        onCancel={() => this.props.goBack()}
        />
        <FilePond files={this.state.files} ref={ref => this.pond = ref} server={filepondUrl} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      token: null,
      newTitle: state.mediaItem.newTitle,
      newDescription: state.mediaItem.newDescription,
      newThumbnail: state.mediaItem.newThumbnail,
      newVideo: state.mediaItem.newVideo,
      newLowQualityAudio: state.mediaItem.newLowQualityAudio,
      newHighQualityAudio: state.mediaItem.newHighQualityAudio,
      newPdf: state.mediaItem.newPdf,
      newRecordedAt: state.mediaItem.newRecordedAt,
      errors: state.mediaItem.errors,
      activeContentWrapper: state.contentWrapper.activeContentWrapper,
      activeMediaItemGroup: state.mediaItemGroup.activeMediaItemGroup,
      previousLocation: state.routerPreviousLocation.previousLocation,
      currentLocation: state.routerPreviousLocation.currentLocation
    };
};
const mapDispatchToProps = {
  ...mediaItemAddActions,
  ...mediaItemActions,
  ...mediaItemGroupActions,
  ...mediaItemGetActions,
  setActiveContentWrapper,
  ...contentWrapperGetActions,
  push,
  goBack
}

export default connect(mapStateToProps, mapDispatchToProps)(AddMediaItemScreen);
