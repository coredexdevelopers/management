import React, {Component} from "react";
import { connect } from 'react-redux';
import { push, goBack } from 'react-router-redux';
import './index.css';
import FilesTable from '../../components/FilesTable';
import * as fileBrowserGetActions from '../../actions/fileBrowserGetActions';
import * as fileBrowserAddActions from '../../actions/fileBrowserAddActions';
import * as fileBrowserActions from '../../actions/fileBrowserActions';
import { FilePond } from 'react-filepond';
import { Grid, Menu, Sticky } from 'semantic-ui-react';

const filepondUrl = process.env.REACT_APP_FILEPOND_HOST;

class FilesTableScreen extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount(){
    this.props.getFilesAsync(this.props.directoryPath);
  }

  componentDidUpdate(prevProps){
    if(prevProps.directoryPath !== this.props.directoryPath){
      this.props.getFilesAsync(this.props.directoryPath);
    }
  }

  handleCopyClick(file, input){
    const url = file.url;
    input.select();
    document.execCommand('copy');
    return false;
  }

  handleOpenDirectory(directory){
    if(!directory.name.startsWith('/')){
      directory.name = '/' + directory.name;
    }
    this.props.setDirectoryPath(directory.name);
  //  this.props.getFilesAsync(this.props.directoryPath);
  }

  handleBackClick(){
    var directoryPath = this.props.directoryPath;
    var index = directoryPath.lastIndexOf('/');
    if(index > 0) { // at index 0 it's the main slash, we don't want to remove it.
      // alert(directoryPath);
      if(!directoryPath.startsWith('/')){
        directoryPath = '/' + directoryPath;
      }
      this.props.setDirectoryPath(directoryPath.substring(0, index));
    } else {
      this.props.setDirectoryPath('/');
    }

  }

  handleAddFileMetadata(file){
    console.log(file);
    file.setMetadata('directoryPath', this.props.directoryPath);
  }

  handleFileUploaded(error){
    if(!error){
      this.props.getFilesAsync(this.props.directoryPath);
    }
  }

  async handleNewFolderClick(){
     var directoryName = prompt("يرجى إدخال اسم المجلّد");
     if(directoryName){
       await this.props.addDirectoryAsync(null, this.props.directoryPath + '/' + directoryName);
       if(this.props.errors.length > 0) {
         alert('تعذر إنشاء المجلد الجديد');
         return;
       }
       alert('تم إنشاء المجلد الجديد بنجاح');
       this.props.getFilesAsync(this.props.directoryPath);
     }

  }

  render() {
    const files = this.props.files;
    const errors = this.props.errors;
    const push = this.props.push;
    return (
      <div className="app-container">
        <Sticky>
          <Menu>
            <Menu.Item icon="arrow left" onClick={() => this.props.goBack()}>
            </Menu.Item>
            <Menu.Item>
              الملفات
            </Menu.Item>
          </Menu>
        </Sticky>
        <Grid>
          <Grid.Column mobile={16} tablet={16} computer={16}>
            <FilesTable files={this.props.files}
              onCopyClick={(file, input) => this.handleCopyClick(file, input)}
              onDirectoryClick={(directory) => this.handleOpenDirectory(directory)}
              onBackClick={() => this.handleBackClick()}
              onNewFolderClick={() => this.handleNewFolderClick()}
            />
          </Grid.Column>
        </Grid>
        <FilePond server={filepondUrl+'/'} onaddfilestart={(file) => this.handleAddFileMetadata(file)}
          onprocessfile={(error, file) => this.handleFileUploaded(error)}
           />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      files: state.fileBrowser.files,
      directoryPath: state.fileBrowser.directoryPath,
      errors: state.fileBrowser.errors
    };
};

const mapDispatchToProps = {
  ...fileBrowserGetActions,
  ...fileBrowserActions,
  ...fileBrowserAddActions,
  push,
  goBack
}

export default connect(mapStateToProps, mapDispatchToProps)(FilesTableScreen);
