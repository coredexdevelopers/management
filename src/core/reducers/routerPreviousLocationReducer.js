import { LOCATION_CHANGE } from 'react-router-redux';

const initialState = {
    previousLocation: null,
    currentLocation: null,
}

export default (state = initialState, action) => {
    switch (action.type) {
        case LOCATION_CHANGE:
            return {
              ...state,
                previousLocation: state.currentLocation,
                currentLocation: action.payload.location.pathname,
            }
        default:
            return state
    }
}
