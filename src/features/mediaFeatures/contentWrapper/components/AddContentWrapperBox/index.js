import React from 'react';
import './index.css';
import { Card, Button } from 'semantic-ui-react';

const AddContentWrapperBox = ({...props}) => {
  return (
    <Card {...props} fluid>
      <Card.Content>
        <Button fluid color="green">
          إضافة مجموعة رئيسية
        </Button>
      </Card.Content>
    </Card>
  );
};


export default AddContentWrapperBox;
