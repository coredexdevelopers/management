import React, {Component} from "react";
import { connect } from 'react-redux';
import { push, goBack } from 'react-router-redux';
import './index.css';
import EditContentWrapperForm from '../../components/EditContentWrapperForm';
import * as contentWrapperActions from '../../actions/contentWrapperActions';
import * as contentWrapperEditActions from '../../actions/contentWrapperEditActions';
import * as contentWrapperGetActions from '../../actions/contentWrapperGetActions';
import { Sticky, Menu, Message, Grid } from 'semantic-ui-react';
import { FilePond } from 'react-filepond';

const filepondUrl = process.env.REACT_APP_FILEPOND_HOST;
class EditContentWrapperScreen extends Component {
  constructor(props) {
    super(props);
    this.state = { saved: 0 };
  }

  componentDidMount(){
    this.props.setActiveContentWrapper(null);
    const contentWrapperId = this.props.match.params.id;
    this.props.getContentWrapperAsync({id: contentWrapperId});
  }

  componentDidUpdate(prevProps){
    if(prevProps.activeContentWrapper !== this.props.activeContentWrapper
    && this.props.activeContentWrapper !== null){
      this.setDefaultValues();
    }
  }

  setDefaultValues(){
    // set default values of active content wrapper
    const contentWrapper = this.props.activeContentWrapper;
    this.props.setNewTitle(contentWrapper.title);
  }

  handleEditContentWrapperAsync(){
    const editContentWrapperAsync = this.props.editContentWrapperAsync;
    editContentWrapperAsync(this.props.token, this.props.activeContentWrapper, this.props.newTitle, null)
    .then(() => {
      this.setState({saved: this.state.saved+1});
    })
  }

  render() {
    const setNewTitle = this.props.setNewTitle;
    const newTitle = this.props.newTitle;
    const goBack = this.props.goBack;
    return (
      <div className="app-container">
        <Sticky>
          <Menu>
            <Menu.Item icon="arrow left" onClick={() => this.props.goBack()}>
            </Menu.Item>
            <Menu.Item>
            تعديل مجموعة رئيسيّة
            </Menu.Item>
          </Menu>
        </Sticky>
        {this.state.saved > 0 &&
          <Message positive>
            {this.state.saved} saved successfully.
          </Message>
        }
      <Grid centered>
        <Grid.Column>
          <EditContentWrapperForm
            fluid
            contentWrapper={this.props.activeContentWrapper}
            onNewTitle={(newTitle) => setNewTitle(newTitle)}
            newTitle={newTitle}
            onSave={() => this.handleEditContentWrapperAsync()}
            onCancel={() => goBack()}
            />
        </Grid.Column>
      </Grid>
      <div>

      </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      activeContentWrapper: state.contentWrapper.activeContentWrapper,
      newTitle: state.contentWrapper.newTitle
    };
};
const mapDispatchToProps = {
    ...contentWrapperActions,
    ...contentWrapperEditActions,
    ...contentWrapperGetActions,
    push,
    goBack
};

export default connect(mapStateToProps, mapDispatchToProps)(EditContentWrapperScreen);
